## Inventory Management App with React

**A React application for managing an inventory of products with CRUD (Create, Read, Update, Delete) functionalities.**

![Inventory Page](./src/assets/image1.png)

### Features

- **Product List:** View a dynamic list of products with details (name, description, price, etc.).
- **Create Product:** Add new products by entering information in a modal window.
- **Update Product:** Edit existing product details through a modal window.
- **Delete Product:** Remove products from the inventory.
- **MUI Integration:** Leverages Material-UI components for a modern and responsive user interface.
- **MUI Icons:** Provides clear visual cues for actions like add, edit, and delete.
- **Modal Component:** Includes a reusable modal component for product creation and editing.

### Technologies

- React
- Axios (for making API requests)
- Material-UI (MUI)
- MUI Icons

### Getting Started

1. **Prerequisites:**

   - Node.js

2. **Clone the Repository:**

   ```bash
   git clone https://gitlab.com/dacost.dev/dacost.dev/react-inventory.git
   ```

3. **Install Dependencies:**

   ```bash
   cd react-inventory
   npm install
   ```

4. **Run the Development Server:**

   ```bash
   npm start
   ```

### Usage

1. Start the development server (`npm start`)
2. Visit `http://localhost:3000/` in your browser.
3. Interact with the product list, create/edit/delete buttons, and modal forms.

### License

This project is licensed under the MIT License (see `LICENSE.md` for details).
