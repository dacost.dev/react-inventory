import { useState, useEffect } from "react";
import axios from "axios";

function useFetch(url, method = "get", options = {}) {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  async function fetchData() {
    try {
      setIsLoading(true);
      const response = await axios({
        url,
        method,
        ...options,
      });
      setData(response.data);
    } catch (error) {
      setError(error.message || "An error occurred");
    } finally {
      setIsLoading(false);
    }
  }

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const refetch = async () => {
    await fetchData();
  };

  return { data, error, isLoading, refetch };
}

export default useFetch;
