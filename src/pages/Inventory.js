import { useState, useCallback } from "react";
import useFetch from "../hooks/useFetch";
import { IconButton, Button, Modal, Alert } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import AddIcon from "@mui/icons-material/Add";
import InventoryIcon from "@mui/icons-material/Inventory";
import ProductForm from "../components/ProductForm";
import axios from "axios";

function Inventory() {
  const { data, error, isLoading, refetch } = useFetch(
    `${process.env.REACT_APP_API_URL}${process.env.REACT_APP_API_VERSION}/product/get_all`
  );

  const [openModal, setOpenModal] = useState(false);
  const [openModalUpdate, setOpenModalUpdate] = useState(false);
  const [currentId, setCurrentID] = useState(0);

  const deleteProduct = useCallback(
    (id) => {
      axios
        .delete(
          `${process.env.REACT_APP_API_URL}${process.env.REACT_APP_API_VERSION}/product/delete/${id}/`
        )
        .then((response) => {
          refetch();
        })
        .catch((error) => {
          Alert(error.message);
        });
    },
    [refetch]
  );

  return (
    <>
      <Modal
        open={openModal}
        onClose={() => setOpenModal(false)}
        aria-labelledby="modal-form"
        sx={{ backgroundColor: "#164863ba" }}
      >
        <>
          <ProductForm setOpenModal={setOpenModal} refetch={refetch} />
        </>
      </Modal>

      <Modal
        open={openModalUpdate}
        onClose={() => setOpenModalUpdate(false)}
        aria-labelledby="modal-form"
        sx={{ backgroundColor: "#164863ba" }}
      >
        <>
          <ProductForm
            setOpenModal={setOpenModalUpdate}
            idItem={currentId}
            refetch={refetch}
          />
        </>
      </Modal>

      <div className="inventory">
        <div className="headerContainer">
          <h1>Product List</h1>
          <Button
            onClick={() => setOpenModal(true)}
            sx={{
              border: "2px solid",
              borderRadius: "21px",
              color: "var(--primary)",
            }}
          >
            <AddIcon />
            Add Product
          </Button>
        </div>
        <div>
          {isLoading ? (
            <div className="errorLoadingContainer">Loading...</div>
          ) : null}
          {error ? (
            <div className="errorLoadingContainer">
              Error getting information, try again later...
            </div>
          ) : null}
          {data ? (
            <div className="dataContainer">
              {data.map((element) => {
                return (
                  <div
                    className="productContainer"
                    key={element.name + element.id}
                  >
                    <div className="productImageContainer">
                      <img
                        className="productImage"
                        src="https://picsum.photos/200/300"
                        alt=""
                      />
                    </div>
                    <div className="productInfoContainer">
                      <div className="productName">{element.name}</div>
                      <div className="productDescription">
                        {element.description}
                      </div>
                      <div className="stockPrice">
                        <div className="productPrice">${element.price}</div>
                        <div className="productQuantity">
                          <InventoryIcon sx={{ width: "15px" }} />
                          {element.quantity}
                        </div>
                      </div>
                    </div>
                    <div className="productActionsContainer">
                      <IconButton
                        sx={{
                          color: " var(--secondary)",
                          textShadow: "1px 1px 2px var(--tertiary)",
                        }}
                        onClick={() => {
                          setCurrentID(element.id);
                          setOpenModalUpdate(true);
                        }}
                      >
                        <EditIcon />
                      </IconButton>
                      <IconButton
                        sx={{
                          color: " var(--secondary)",
                          textShadow: "1px 1px 2px var(--tertiary)",
                        }}
                        onClick={() => deleteProduct(element.id)}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </div>
                  </div>
                );
              })}
            </div>
          ) : null}
          {data && data.length === 0 && !error ? (
            <div className="errorLoadingContainer">No products available</div>
          ) : null}
        </div>
      </div>
    </>
  );
}

export default Inventory;
