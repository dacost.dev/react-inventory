import React, { useEffect, useState } from "react";
import useFetch from "../hooks/useFetch";
import axios from "axios";
import { Alert, Button } from "@mui/material";
import CancelIcon from "@mui/icons-material/Cancel";
import SendIcon from "@mui/icons-material/Send";

function ProductForm(props) {
  const {
    data: elementById,
    error: errorElementById,
    isLoading: isLoadingElementById,
  } = useFetch(
    `${process.env.REACT_APP_API_URL}${process.env.REACT_APP_API_VERSION}/product/get_by_id/${props.idItem}/`
  );

  const [name, setName] = useState(" ");
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState("");
  const [quantity, setQuantity] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    if (elementById && !errorElementById && !isLoadingElementById) {
      setName(elementById.name);
      setPrice(elementById.price);
      setDescription(elementById.description);
      setQuantity(elementById.quantity);
    }
  }, [elementById, error, errorElementById, isLoading, isLoadingElementById]);

  const validateForm = () => {
    let isValid = true;
    setError(null);

    if (!name.trim()) {
      setError("Name is required.");
      isValid = false;
    } else if (!/^[a-zA-Z\s]+$/.test(name)) {
      setError("Name can only contain letters and spaces.");
      isValid = false;
    }

    if (price <= 0) {
      setError("Price must be a positive number.");
      isValid = false;
    }

    if (description.length > 200) {
      setError("Description cannot exceed 200 characters.");
      isValid = false;
    }

    if (quantity < 0) {
      setError("Quantity must be a non-negative number.");
      isValid = false;
    }

    return isValid;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!validateForm()) {
      return;
    }

    setIsLoading(true);

    try {
      if (props.idItem) {
        await axios
          .put(
            `${process.env.REACT_APP_API_URL}${process.env.REACT_APP_API_VERSION}/product/update`,
            {
              id: props.idItem,
              name,
              price,
              description,
              quantity,
            }
          )
          .then((response) => {
            console.log(response);
          })
          .catch((error) => {
            Alert(error.message);
          })
          .finally(() => {
            setName("");
            setPrice(0);
            setDescription("");
            setQuantity(0);
            props.setOpenModal(false);
            props.refetch();
          });
      } else
        await axios
          .post(
            `${process.env.REACT_APP_API_URL}${process.env.REACT_APP_API_VERSION}/product/create`,
            {
              name,
              price,
              description,
              quantity,
            }
          )
          .then((response) => {
            console.log(response);
          })
          .catch((error) => {
            Alert(error.message);
          })
          .finally(() => {
            setName("");
            setPrice(0);
            setDescription("");
            setQuantity(0);
            props.setOpenModal(false);
            props.refetch();
          });
    } catch (error) {
      if (error.response?.status === 400) {
        Alert(
          error.response.data.error ||
            "An error occurred while saving the product."
        );
      } else {
        Alert(error.message || "An error occurred while saving the product.");
      }
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="productFormContainer">
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Product Name"
            required
          />
          {error && error.startsWith("Name") && (
            <p className="error">{error}</p>
          )}
        </div>

        <div className="form-group">
          <label htmlFor="price">Price</label>
          <input
            type="number"
            step="0.01"
            id="price"
            value={price}
            onChange={(e) => setPrice(Number(e.target.value))}
            placeholder="Price"
            required
          />
          {error && error.startsWith("Price") && (
            <p className="error">{error}</p>
          )}
        </div>

        <div className="form-group">
          <label htmlFor="quatity">Quantity</label>
          <input
            type="number"
            id="quantity"
            value={quantity}
            onChange={(e) => setQuantity(Number(e.target.value))}
            placeholder="Quantity"
            required
          />
          {error && error.startsWith("Quantity") && (
            <p className="error">{error}</p>
          )}
        </div>

        <div className="form-group">
          <label htmlFor="description">Description</label>
          <textarea
            id="description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Product Description"
            required
          />
          {error && error.startsWith("Description") && (
            <p className="error">{error}</p>
          )}
        </div>

        <div className="formBtn">
          <Button
            type="submit"
            disabled={isLoading || error}
            sx={{
              border: "2px solid",
              borderRadius: "21px",
              color: "var(--tertiary)",
            }}
          >
            <SendIcon />
            {isLoading ? "Saving..." : "Save"}
          </Button>

          <Button
            type="button"
            onClick={() => props.setOpenModal(false)}
            disabled={isLoading || error}
            sx={{
              border: "2px solid",
              borderRadius: "21px",
              color: "var(--quaternary)",
            }}
          >
            <CancelIcon />
            Cancel
          </Button>
        </div>
      </form>
    </div>
  );
}

export default ProductForm;
