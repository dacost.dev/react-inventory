import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Inventory } from "../pages";

function RouterApp() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Inventory />} />
      </Routes>
    </BrowserRouter>
  );
}

export default RouterApp;
